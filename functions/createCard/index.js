'use strict';
const { NumberByLength, NumberRnd, validateAgeWithCondition } = require('../../helpers');

const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async (event) => {
  const data = JSON.parse(event.Records[0].body);
  const body = JSON.parse(data.Message);
  const { dni, birthDate } = body;

  const typeCard = validateAgeWithCondition(birthDate, 'card');
  const cardNumber = NumberByLength(16);
  const ccvNumber = NumberByLength(3);
  const expirationDate = `${NumberRnd(1, 12)}/${NumberRnd(23, 33)}`;

  const dbParams = {
    TableName: process.env.CLIENTS_TABLE,
    ExpressionAttributeNames: {
      '#C': 'creditCard',
    },
    ExpressionAttributeValues: {
      ':c': {
        ccv: ccvNumber,
        expirationDate: expirationDate,
        cardNumber: cardNumber,
        type: typeCard,
      },
    },
    UpdateExpression: 'SET #C = :c',
    ReturnValues: 'ALL_NEW',
    Key: {
      dni,
    },
  };
  const dbClient = await db.update(dbParams).promise();
  console.log('dbClient', dbClient);

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Card Created!' }),
  };
};

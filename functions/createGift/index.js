'use strict';
const { giftByBirthDate } = require('../../helpers');

const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async (event) => {
  const data = JSON.parse(event.Records[0].body);
  const body = JSON.parse(data.Message);
  const { dni, birthDate } = body;
  const gift = giftByBirthDate(birthDate);

  const dbParams = {
    TableName: process.env.CLIENTS_TABLE,
    ExpressionAttributeNames: {
      '#G': 'gift',
    },
    ExpressionAttributeValues: {
      ':g': gift,
    },
    UpdateExpression: 'SET #G = :g',
    ReturnValues: 'ALL_NEW',
    Key: {
      dni,
    },
  };

  const dbClient = await db.update(dbParams).promise();
  console.log('dbClient', dbClient);
  try {
    return {
      statusCode: 200,
      body: JSON.stringify('Gift successfully registered'),
    };
  } catch (error) {
    const response = {
      statusCode: 500,
      body: 'Unexpected error!',
    };
    return response;
  }
};

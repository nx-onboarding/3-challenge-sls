'use strict';
const { createClient, getClient, publishSNS, validateAgeWithCondition } = require('../../helpers');

module.exports.handler = async (event) => {
  const body = JSON.parse(event.body);
  if (!body.dni || !body.name || !body.lastName || !body.birthDate) {
    return {
      statusCode: 400,
      body: JSON.stringify('Must include all the items'),
    };
  }

  const existClient = await getClient(body.dni);

  if (existClient) {
    return {
      statusCode: 400,
      body: JSON.stringify('Customer already exists'),
    };
  }

  if (validateAgeWithCondition(body.birthDate)) {
    return {
      statusCode: 400,
      body: JSON.stringify('Must be between 18 and 65 years old'),
    };
  }

  await createClient(body);
  await publishSNS(body);

  try {
    return {
      statusCode: 200,
      body: JSON.stringify('Client successfully registered'),
    };
  } catch (error) {
    const response = {
      statusCode: 500,
      body: 'Unexpected error!',
    };
    return response;
  }
};

'use strict';

const AWS = require('aws-sdk');
const db = new AWS.DynamoDB.DocumentClient();
const sns = new AWS.SNS();

const params = {
  TableName: process.env.CLIENTS_TABLE,
};

const getClient = async (dni) => {
  const client = await db
    .get({
      Key: {
        dni,
      },
      ...params,
    })
    .promise();
  try {
    return Object.keys(client).length === 0 ? null : client;
  } catch (error) {
    return error;
  }
};

const validateAgeWithCondition = (date, condition = null) => {
  const today = new Date();
  const birthDate = new Date(date);
  const age = today.getFullYear() - birthDate.getFullYear();
  if (condition === 'card') {
    return age < 45 ? 'Classic' : 'Gold';
  }
  return age < 18 || age > 65;
};

const createClient = async (client) => {
  const { dni, name, lastName, birthDate } = client;
  const _params = {
    ...params,
    Item: {
      dni,
      name,
      lastName,
      birthDate,
    },
    ReturnConsumedCapacity: 'TOTAL',
  };

  await db.put(_params).promise();
};

const publishSNS = async (event) => {
  const snsParams = {
    Message: JSON.stringify(event),
    TopicArn: process.env.SNS_CLIENTS_TOPIC,
  };

  const snsPublished = await sns.publish(snsParams).promise();
  console.log('snsPublished', snsPublished);
};

const NumberByLength = (length = 16) => {
  return Math.random()
    .toString()
    .substring(2, length + 2);
};

const NumberRnd = (minimum, maximum) => {
  const result = Math.round(Math.random() * (maximum - minimum) + minimum);
  return result < 10 ? `0${result}` : result;
};

const giftByBirthDate = (birthday) => {
  const month = Number(birthday.split('-')[1]);
  if (3 <= month && month <= 5) {
    return 'buzo';
  }

  if (6 <= month && month <= 8) {
    return 'sweater';
  }

  if (9 <= month && month <= 11) {
    return 'camisa';
  }

  return 'remera';
};

module.exports = {
  createClient,
  getClient,
  giftByBirthDate,
  NumberByLength,
  NumberRnd,
  publishSNS,
  validateAgeWithCondition,
};

# Serverless

## Ejercicio practico:

### Arquitectura:

- Crear yaml serverless:
  - API Gateway: ejecuta a la Lambda de creatClient
  - Lambda createClient: publica SNS clientCreated
  - SNS clienteCreated: envia un mensaje a los SQS createCard y createCard
  - SQS createCard ejecuta a la Lambda createCard
  - SQS createGift ejecuta a la Lambda createCard

### Features:

- create-client: carga clientes con dni, nombre, apellido y fecha de nacimiento. Solo debe permitir mayores de edad hasta 65 años
- create-card: otorga una tarjeta de crédito (Classic si es menor a 45 años y Gold si es mayor) generando número, vencimiento y código de seguridad.
- create-gift: asigna un regalo de cumpleaños según la estación en la que cae, variando entre buzo si es otoño, sweater si es invierno, camisa si es primavera y remera si es verano.

### Endpoint

- <b>url:</b> https://5x1rum70fe.execute-api.us-east-1.amazonaws.com/dev/client
- <b>method:</b> POST
- <b>body:</b>

```json
{
  "dni": "1234567920",
  "name": "Pedro",
  "lastName": "Pérez",
  "birthDate": "1997-10-01"
}
```
